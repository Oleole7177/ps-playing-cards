require 'card' # will add /lib to this path as default
require 'set'

describe Card, :unit do

  def card(params = {})
    defaults = {
      suit: :hearts,
      rank: 7,
    }

    Card.build(*defaults.merge(params).values_at(:suit, :rank)) # ** convert hash into named parameters
  end

  it 'has a suit' do
    # raise unless card(suit: :spades).suit == :spades
    expect(card(suit: :spades).suit).to eq(:spades)
  end

  it 'has a rank' do
    expect(card(rank: 4).rank).to eq(4)
  end

  context 'equality' do
    subject { card(suit: :spades, rank: 4)}

    describe 'comparing against itself' do
      def other
        @other ||= card(suit: :spades, rank: 4)
      end

      it 'is equal to itself' do
        expect(subject).to eq(other)
      end

      it 'is hash equal to itself' do
        expect(Set.new([subject, other]).size).to eq(1)
      end
    end

    shared_examples_for 'an unequal card' do
      it 'is not equal' do
        expect(subject).not_to eq(other)
      end

      it 'is not hash equal' do
        expect(Set.new([subject, other]).size).to eq(2)
      end
    end

    describe 'comparing to a card of different suit' do
      let(:other) { card(suit: :hearts, rank: 4) }

      it_behaves_like 'an unequal card'
    end


    describe 'comparing to a card of different rank' do
      let(:other) { card(suit: :spades, rank: 5) }

      it_behaves_like 'an unequal card'
    end
  end

  describe 'a jack' do
    it 'ranks higher than a 10' do
      lower = card(rank: 10)
      higher = card(rank: :jack)

      raise unless higher.rank > lower.rank
    end
  end

  describe 'a queen' do
    it 'ranks higher than a jack' do
      lower = card(suit: :spades, rank: :jack)
      higher = card(suit: :spades, rank: :queen)

      raise unless higher.rank > lower.rank
    end
  end

  describe 'a king' do
    it 'ranks higher than a queen' do
      lower = card(suit: :spades, rank: :queen)
      higher = card(suit: :spades, rank: :king)

      raise unless higher.rank > lower.rank
    end
  end

  describe '.from_string', :aggregate_failures do
    # it 'parses numbers' do
    #   expect(Card.from_string("7H")).to eq(Card.build(:hearts, 7))
    # end
    #
    # it 'parses 10' do
    #   expect(Card.from_string("10S")).to eq(Card.build(:spades, 10))
    # end
    #
    # it 'parses face cards' do
    #   # aggregate_failures do - еще один способ использования
    #     expect(Card.from_string("JC")).to eq(Card.build(:clubs, :jack))
    #     expect(Card.from_string("QC")).to eq(Card.build(:clubs, :queen))
    #     expect(Card.from_string("KC")).to eq(Card.build(:clubs, :king))
    #   # end
    # end

    # другой способ описать все это же - custom example builder, в котором мы можем
    # динамически определить нашу спеку

    def self.it_parses(string, as: as)
      it "parses #{string}" do
        expect(Card.from_string(string)).to eq(as)
      end
    end

    it_parses "7H", as: Card.build(:hearts, 7)
    it_parses "10S", as: Card.build(:spades, 10)
    it_parses "QC", as: Card.build(:clubs, :jack)

  end

end

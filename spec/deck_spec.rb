require 'deck'

# RSpec::Matchers.define(:be_contigious) do
#   match do |array|
#     array
#       .sort
#       .each_cons(2)
#       .all? { |x, y| x + 1 == y }
#   end
# end


# custom matcher with custom failure message
# RSpec::Matchers.define(:be_contigious_by_attribute) do |attr|
#   match do |array|
#     !first_non_contigious_pair(array, attr)
#   end
#
#   failure_message do |array|
#     "%s and %s were not contigious" % first_non_contigious_pair(array, attr)
#   end
#
#   def first_non_contigious_pair(array, attr)
#     array
#       .sort_by { |x| x.send(attr) }
#       .each_cons(2)
#       .detect { |x, y| x.send(attr) + 1 != y.send(attr) }
#   end
# end


# more generic custom matcher - it's bad practice do define matcher in global scope
# so use Array module
module ArrayMatchers
  extend RSpec::Matchers::DSL

  matcher :be_contigious_by do
    match do |array|
      !first_non_contigious_pair(array)
    end

    failure_message do |array|
      "%s and %s were not contigious" % first_non_contigious_pair(array)
    end

    def first_non_contigious_pair(array)
      array
        .sort_by(&block_arg)
        .each_cons(2)
        .detect { |x, y| block_arg.call(x) + 1 != block_arg.call(y) }
    end
  end
end

describe 'Deck' do
  include ArrayMatchers
  describe '.all' do
    it 'contains 32 cards' do
      expect(Deck.all.length).to eq(32)
    end

    it 'has a seven its lowest card' do
      # expect(Deck.all.map { |card| card.rank }).to all(be >= 7)
      # а таким образом мы протестим еще то, что у карты в колоде есть атрибут rank:
      expect(Deck.all).to all(have_attributes(rank: be >= 7))
    end

    it 'has contigious ranks by suit' do
      # expect(Deck.all.group_by { |card| card.suit }.each do |suit, cards|
      #   expect(cards).to be_contigious_by_atribute(:rank)
      # end
      expect(Deck.all.group_by { |card| card.suit }.values).to \
        all(be_contigious_by(&:rank))

    end
  end
end

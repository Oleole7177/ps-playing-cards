# это просто пример того, как можно сымитировать работу
# Mock-ов. То есть у нас есть массив expectaions, есть массиы вызовов.
# и мы просто проверяем будут ли они равны (подробнее смотри ниже)
# в комментах - как это сделать самому

require 'rspec/mocks'

include ::RSpec::Mocks::ExampleMethods # позволяет пользоваться expect, recieve


::RSpec::Mocks.setup
# def setup_expectation(obj, method)
#   $expectations << [obj, method]
#   obj.singleton_class.send(:define_method, method) do
#     $calls[obj] << method
#   end
# end

# Mock setup
# $expectations = []
# $calls = Hash.new { |h,k| h[k] = [] }

begin
  # Test
  obj = Object.new
  # setup_expectation(obj, :hello)
  expect(obj).to receive(:hello)
  obj.hello

  # Mock verification
  ::RSpec::Mocks.verify
  # $expectations.each do |obj, method|
  #   unless $calls[obj].include?(method)
  #     raise "#{obj} did not recieve #{method}"
  #   end
  # end
ensure
  ::RSpec::Mocks.teardown
  # $expectations.each do |obj, method|
  #   obj.singleton_class.send(:undef_method, method)
  # end
end

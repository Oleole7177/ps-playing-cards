# example how to test cli interactivity
# give us confidence that app will run
# WE HAVE NO THIS FUNCTIONALITY HERE, THIS SPECS ARE JUST EXAMPLES
require 'pty'

BIN = File.expand_path("../../bin/play", __FILE__)

describe 'CLI', :acceptance do
  #  a method to setup our envitonment
  def run_app(&block)
    dir = Dir.tmpdir + '/highcard_test_state'
    "rm -Rf #{dir}"
    "mkdir -p #{dir}"
    ENV['HIGHCARD_DIR'] = dir
    PTY.spawn(BIN, &block)
  end

  example 'it works' do
    run_app do |output, input, pid|
      sleep 0.5

      input.write("y\n")

      sleep 0.5

      buffer = output.read_nonblock(1024)
      # raise unless buffer.include?("You won") || buffer.include?("You lost")
      # можно сделать chain из expect
      expect(buffer).to include?("You won").or include("You lost")
    end
  end

  example 'betting on winning' do
    run_app do |output, input, pid|
      sleep 0.5

      input.write("y\n")

      sleep 0.5

      buffer = output.read_nonblock(1024)
      raise unless buffer.include?("You won") || buffer.include?("You lost")
    end
  end

end
